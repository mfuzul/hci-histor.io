import React from "react";
import { graphql } from "gatsby";

import styles from "./styles.module.css";

export default ({ data }) => {
    const { markdownRemark: post } = data;

    return (
        <div className={styles.Blog_Card}>
            <h1 className={styles.Blog_Title}>{post.frontmatter.title}</h1>
            <section
                className={styles.Post}
                dangerouslySetInnerHTML={{ __html: post.html }}
            />
        </div>
    );
};

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date
        slug
        title
      }
    }
  }
`;