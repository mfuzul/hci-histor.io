import React from "react";
import {graphql} from "gatsby";
import Img from "gatsby-image";

import "./styles.css";

const Museum = ({data}) => {
    const {markdownRemark: museum} = data;
    return (
        <div className="museum-container">
            <div className="museum-image">
                <Img fluid={museum.frontmatter.indexImage.childImageSharp.fluid}/>
            </div>
            <div className="museum-info">
                <span className="museum-title">{museum.frontmatter.name}</span>
                <table className={"work-table"}>
                    <thead>
                    <tr>
                        <th colSpan="2">Working hours</th>
                    </tr>
                    <tr>
                        <th>Day</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Monday</td>
                        <td>08:00 - 20:00</td>
                    </tr>
                    <tr>
                        <td>Tuesday</td>
                        <td>08:00 - 20:00</td>
                    </tr>
                    <tr>
                        <td>Wednesday</td>
                        <td>08:00 - 20:00</td>
                    </tr>
                    <tr>
                        <td>Thursday</td>
                        <td>08:00 - 20:00</td>
                    </tr>
                    <tr>
                        <td>Friday</td>
                        <td>08:00 - 20:00</td>
                    </tr>
                    <tr>
                        <td>Saturday</td>
                        <td>08:00 - 14:00</td>
                    </tr>
                    <tr>
                        <td>Sunday</td>
                        <td>08:00 - 14:00</td>
                    </tr>
                    </tbody>
                </table>
                <div className={"museum-buy"}>
                    <span className={"museum-price"}><i className={'fa fa-money'}></i> { museum.frontmatter.price }</span>
                    <a href={`/buy?museum=${museum.frontmatter.slug}`} className="Btn buy-btn">Buy ticket</a>
                </div>
            </div>
            <div className="museum-description">
                <h3>About</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum tempor efficitur. Sed vitae hendrerit
                sapien. Donec eget quam enim. Maecenas aliquam egestas congue. Morbi laoreet varius urna, non elementum
                erat tempus quis. Quisque eget leo ut erat ultrices cursus. Vestibulum vitae justo eget lorem congue
                dignissim ornare nec purus. Nam faucibus nisl ligula, in pulvinar ante mollis quis.
                Vivamus magna turpis, ultricies sed luctus ut, placerat vel dolor. Ut sed nisl sed nulla maximus rutrum
                at ac mi. Aenean a pellentesque erat. Fusce interdum leo risus, eget luctus lacus rhoncus ac. Nulla ac
                aliquet nisl, et cursus purus. Cras quis lorem facilisis, fringilla lorem vitae, tempus risus. Etiam
                porttitor turpis vel elit imperdiet elementum sit amet ac elit. Quisque lacinia erat quis eros posuere,
                eu laoreet dolor tincidunt. Vestibulum enim velit, placerat eu pharetra ac, porttitor non velit.
                Curabitur pellentesque ac enim in fringilla. Ut sagittis tincidunt porta. Vestibulum ante ipsum primis
                in faucibus orci luctus et ultrices posuere cubilia Curae;

                Donec congue, lorem imperdiet mattis tincidunt, urna tortor sodales nisi, at fringilla purus ante in
                elit. Quisque id neque tempus, fringilla diam sit amet, sollicitudin metus. Maecenas ex enim, accumsan
                et condimentum et, volutpat vitae erat. Sed auctor erat nec ex tristique rutrum. Sed laoreet maximus
                condimentum. Donec ac dignissim massa, ac maximus ligula. Nulla nec consectetur velit. Morbi non lectus
                non ipsum tristique molestie a eget massa. Vivamus nec arcu in ipsum ullamcorper feugiat et ut erat.
                Curabitur nec gravida mauris, sit amet malesuada lectus. Vestibulum molestie, orci sit amet lacinia
                rhoncus, libero massa facilisis lectus, at euismod est quam et massa. Aliquam erat volutpat. Vivamus
                sagittis, magna non elementum consequat, quam nulla pellentesque dui, in lacinia orci nisl eu arcu.
                Donec in luctus purus, ut vestibulum est. Donec felis odio, lacinia eget viverra quis, blandit nec nisl.
                    Vivamus non vulputate lorem.</p>
            </div>
        </div>
    );
};

export default Museum;

export const query = graphql`
    query MuseumQuery($slug: String!) {
        markdownRemark(frontmatter: { slug: { eq: $slug } }) {
            frontmatter {
                name
                city
                price
                slug
                indexImage {
                    childImageSharp {
                      fluid {
                        ...GatsbyImageSharpFluid
                      }
                    }
                }
            }
        }
    }
`;