import React from "react";
import styles from '../styles/pages/search-results.module.css';

import { graphql } from "gatsby";
import * as jsSearch from 'js-search';
import * as moment from 'moment';
import { MuseumResults, MuseumResultHeader } from "../components";

class SearchPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let { data: { allMarkdownRemark: { museums }}, location } = this.props;

        museums = museums.map(museumData => {
            return {
                id: museumData.museum.data.id,
                name: museumData.museum.data.name,
                city: museumData.museum.data.city,
                price: museumData.museum.data.price,
                ticketsLeft: museumData.museum.data.ticketsLeft,
                slug: museumData.museum.data.slug
            };
        });

        this.search = new jsSearch.Search('id');

        this.search.addIndex('city');
        this.search.addDocuments(museums);

        let museumResultHeaderComponent = '';
        const searchParams = this.prepareSearchParams(location);

        if (null !== searchParams &&
            searchParams.hasOwnProperty('city') &&
            searchParams.hasOwnProperty('date')
        ) {
            museums = this.search.search(searchParams.city);
            if ('' === searchParams.date) {
                searchParams.date = moment().format('DD. MM. YYYY');
            }

            museumResultHeaderComponent = (
                <MuseumResultHeader
                    city={searchParams.city.replace('+', ' ')}
                    date={searchParams.date}
                />
            );
        }

        return (
            <>
                <div className={styles.Search_Results_Wrapper}>
                    <div className={styles.Header_Wrapper}>
                        {museumResultHeaderComponent}
                    </div>
                    <div className={styles.Results_Wrapper}>
                        <MuseumResults museums={museums}/>
                    </div>
                </div>
            </>
        );
    }

    prepareSearchParams(location) {
        if ('' === location.search.trim()) {
            return null;
        }

        let params = {};

        location.search
            .slice(1)
            .split('&')
            .forEach(paramString => {
                const [key, value] = paramString.split('=');

                params[key] = value;
            });

        return params;
    }
}

export const query = graphql`
    query allMuseums
    {
      allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/\\/museums\\//" } }) {
        museums: edges{
          museum: node {
            data: frontmatter {
              id
              name
              city
              price
              slug
            }
          }
        }
      }
    }
`;

export default SearchPage;