import React from "react";
import styles from "../styles/pages/buy.module.css";
import cn from "classnames";

import _ from '../components/Form/index';
import {graphql, Link} from "gatsby";
import moment from "moment";

class CustomerRow extends React.Component {
    render() {
        const removeBtn = (<a href={'#'} className={cn('Btn', styles.Remove)} onClick={this.props.onClick}><i className={'fa fa-trash'}></i> &nbsp; Remove</a>);

        return (
            <div className={styles.Customer_Row}>
                <_.Input
                    placeholder={'First name'}
                    className={styles.First_Name}
                />
                <_.Input
                    placeholder={'Last name'}
                    className={styles.Last_Name}
                />
                <_.Select className={styles.Group}>
                    <option>Adult</option>
                    <option>Child</option>
                </_.Select>
                {!this.props.isFirst && removeBtn}
            </div>
        );
    }
}

class Buy extends React.Component {
    constructor(props) {
        super(props);

        this.handleAddPerson = this.handleAddPerson.bind(this);

        this.state = {
            currentId: 0,
            customerRows: [(<CustomerRow key={0} onClick={this.handleRemovePerson} isFirst={true}/>)]
        };
    }

    prepareSearchParams() {
        const { location: { search }} = this.props;

        let paramsObj = {};

        search.slice(1).split('&').forEach(param => {
           let keyValueArray = param.split('=');

           paramsObj[keyValueArray[0]] = keyValueArray[1];
        });

        if (undefined === paramsObj.date) {
            paramsObj.date = (new moment()).format('DD.MM.YYYY.');
        }

        return paramsObj;
    }

    handleAddPerson(e) {
        e.preventDefault();

        const newId = this.state.currentId + 1;

        this.setState({
            currentId: newId,
            customerRows: [...this.state.customerRows, (<CustomerRow key={newId} onClick={this.handleRemovePerson}/>)]
        });
    }

    handleRemovePerson(e) {
        e.preventDefault();

        e.currentTarget.parentNode.remove();
    }

    render() {
        const params = this.prepareSearchParams();
        const {allMarkdownRemark: {museums}} = this.props.data;
        const museumData = museums.find(museumData => {
            return museumData.museum.data.slug === params.museum;
        });

        let museum = {
            slug: '',
            name: '',
            city: ''
        };
        if (undefined !== museumData) {
            museum = museumData.museum.data;
        }

        const {customerRows} = this.state;
        const {handleAddPerson} = this;

        return (
            <div className={styles.Buy_Container}>
                <span className={styles.Visiting}>
                    Visiting <Link to={`/museum/${museum.slug}`}>{museum.name}</Link> in <span>{museum.city}</span> on <span>{params.date}</span>
                    <br/><span>Price: {museum.price}</span>
                </span>
                <div className={styles.E_Mail_Row}>
                    <_.Input
                        type={'e-mail'}
                        placeholder={'E-mail'}
                        className={styles.E_Mail}
                    />
                    <a href={'#'} className={cn('Btn', styles.Add_Person)} onClick={handleAddPerson}>
                        <i className={'fa fa-plus'}></i> &nbsp; Add person
                    </a>
                </div>

                {customerRows}

                <_.Select
                    label={'Payment method'}
                >
                    <option>PayPal</option>
                    <option>MasterCard</option>
                    <option>VISA</option>
                </_.Select>
                <Link to={'/'} className={cn('Btn', styles.Confirm)}>
                    <i className={'fa fa-send'}></i> &nbsp; Confirm
                </Link>
            </div>
        );
    }
}

export default Buy;

export const query = graphql`
    {
      allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/\\/museums\\//" } }) {
        museums: edges{
          museum: node {
            data: frontmatter {
              id
              name
              city
              price
              slug
            }
          }
        }
      }
    }
`;