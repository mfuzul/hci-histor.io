import React from "react";
import styles from "../styles/pages/private.module.css";
import cn from "classnames";
import { PrivateRoute } from "../components";
import { Link, navigate } from "gatsby";
import { Router } from "@reach/router";
import MyTickets from "../components/Private/pages/MyTickets.js";
import {
    handleLogin,
    isLoggedIn
} from "../services/authentication/auth.js";
import LoginForm from "../components/Forms/Login";

const Logout = () => (
    <div className={styles.Logout_Container}>
        <h3>Logged in as <b>John Doe</b>.</h3>
    </div>
);

export default () => (
    <div className={styles.Private_Container}>
        {isLoggedIn() ? (
            <Logout/>
        ) : (
            <>
                <LoginForm private={'/private/my-tickets'} handleLogin={handleLogin} />
            </>
        )}

        <Router>
            <PrivateRoute
                path={'/private/my-tickets'}
                render={MyTickets}
                redirect={'/private'}
            />
        </Router>
    </div>
);