import React from 'react'
import { Search } from "../components";
import styles from '../styles/pages/index.module.css';
import cn from "classnames";

const Index = () => (
    <>
        <Search />
        <div className={styles.Content}>
            <div className={cn(styles.Info_Card, styles.Info_Card_Left)}>
                <span className={styles.Info_Icon}>
                    <i className={cn('fa', 'fa-book', 'fa-4x')}></i>
                </span>
                <span className={styles.Info_Body}>
                    Histor.io gives you the opportunity to experience history of any place you visit.
                    We provide you the most correct and freshest information related to the places you visit.
                </span>
            </div>
            <div className={cn(styles.Info_Card, styles.Info_Card_Right)}>
                <span className={styles.Info_Icon}>
                    <i className={cn('fa', 'fa-envelope', 'fa-4x')}></i>
                </span>
                <span className={styles.Info_Body}>
                    Every ticket you buy through Histor.io is sent directly to your e-mail embedded with a
                    QR code so there is no need to print you ticket. You just show it at the entrance of the museum and
                    you're ready for your adventure.
                </span>
            </div>
        </div>
    </>
);

export default Index
