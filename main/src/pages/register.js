import React from "react";

import RegisterForm from '../components/Forms/Register/index';

export default () => (
    <>
        <RegisterForm />
    </>
);