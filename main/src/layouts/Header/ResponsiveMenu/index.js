import React from "react";
import styles from "./styles.module.css";
import {navigate} from "gatsby";
import NavLink from "../NavLink";
import { PrivateRoute } from "../../../components";
import {handleLogout, isLoggedIn} from "../../../services/authentication/auth";

class ResponsiveMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    redirectOnLogout() {
        navigate('/');
    }

    render() {
        const { redirectOnLogout } = this;
        const { showMenu } = this.props;
        const menu = require('../config.json');

        const navLinksComponent = menu.map(link => {
            if (link.hideOnLogIn && isLoggedIn()) {
                return;
            }

            if (link.private) {
                return (
                    <PrivateRoute
                        key={link.url}
                        render={() => (
                            <NavLink
                                key={link.url}
                                to={link.url}
                                className={styles.NavLink_Private}
                                activeClassName={styles.NavLink_Private_active}
                            >
                                <i className={`fa fa-${link.icon}`}></i>
                                {link.name}
                            </NavLink>
                        )}
                    />
                );
            }


            return (
                <NavLink
                    key={link.name}
                    to={link.url}
                    className={styles.NavLink}
                    activeClassName={styles.NavLink_active}
                >
                    <i className={`fa fa-${link.icon}`}></i>
                    {link.name}
                </NavLink>
            );
        });

        if (isLoggedIn()) {
            navLinksComponent.push((
                <div
                    className={styles.NavLink}
                    onClick={() => handleLogout(redirectOnLogout)}
                >
                    <i className={'fa fa-sign-out'}></i>
                    Logout
                </div>
            ));
        }

        return (
            <div className={showMenu ? styles.Menu_Slide_In : styles.Menu_Slide_Out}>
                {navLinksComponent}
                <span className={styles.Close_Menu}>
                    <i className={'fa fa-times fa-2x'}></i>
                    Press to close menu
                </span>
            </div>
        );
    }
}

export default ResponsiveMenu;