import React from "react";
import { Link, navigate } from "gatsby";
import Nav from "./Nav";
import NavLink from "./NavLink";
import styles from "./styles.module.css";
import { PrivateRoute } from "../../components";
import {handleLogout, isLoggedIn} from "../../services/authentication/auth";

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.handleMenuClick = this.handleMenuClick.bind(this);
    }

    redirectOnLogout() {
        navigate('/');
    }

    handleMenuClick() {
        this.props.toggleMenu();
    }

    render() {
        const { handleMenuClick, redirectOnLogout } = this;
        const menu = require('./config.json');

        const navLinksComponent = menu.map(link => {
            if (link.hideOnLogIn && isLoggedIn()) {
                return;
            }

            if (link.private) {
                return (
                    <PrivateRoute
                        key={link.url}
                        render={() => (
                            <NavLink
                                key={link.url}
                                to={link.url}
                                className={styles.NavLink_Private}
                                activeClassName={styles.NavLink_Private_active}
                            >
                                <i className={`fa fa-${link.icon}`}></i>
                                {link.name}
                            </NavLink>
                        )}
                    />
                );
            }

            return (
                <NavLink key={link.url} to={link.url}>
                    <i className={`fa fa-${link.icon}`}></i>
                    {link.name}
                </NavLink>
            );
        });

        if (isLoggedIn()) {
            navLinksComponent.push((
                <div
                    className={styles.NavLink}
                    onClick={() => handleLogout(redirectOnLogout)}
                >
                    <i className={'fa fa-sign-out'}></i>
                    Logout
                </div>
            ));
        }

        return (
            <header className={styles.Header}>
                <div className={styles.Nav}>
                    <Link to={'/'} className={styles.Navbar_brand}>Histor.io</Link>
                    <Nav className={styles.Right_Nav}>
                        {navLinksComponent}
                    </Nav>
                    <span className={styles.Menu_Btn}
                          onClick={handleMenuClick}
                    >Menu</span>
                </div>
            </header>
        );
    }
}

export default Header;