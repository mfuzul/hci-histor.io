import React from "react";
import styles from "./styles.module.css";
import ResponsiveMenu from "../ResponsiveMenu";

class Overlay extends React.Component {
    constructor(props) {
        super(props);

        this.handleOverlayClick = this.handleOverlayClick.bind(this);
    }

    handleOverlayClick() {
        this.props.toggleMenu();
    }

    render() {
        const { handleOverlayClick } = this;
        const { showMenu } = this.props;

        return (
            <div
                className={showMenu ? styles.Overlay_Fade_In : styles.Overlay_Fade_Out}
                onClick={handleOverlayClick}
            >
                <ResponsiveMenu showMenu={showMenu} />
            </div>
        );
    }
}

export default Overlay;