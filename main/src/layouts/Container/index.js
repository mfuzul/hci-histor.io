import React from 'react';
import cn from 'classnames';
import styles from './styles.module.css';

const Container = ({ className, children }) => (
    <div className={cn(className, styles.Container)}>
        { children }
    </div>
);

export default Container;