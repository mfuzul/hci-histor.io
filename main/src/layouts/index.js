import React from "react";
import Layout from "./Layout";

import '../styles/index.css';

export default ({children}) => (
    <Layout>
        {children}
    </Layout>
);