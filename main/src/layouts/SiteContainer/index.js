import React from "react";
import "../../components/reset.css";
import styles from "./styles.module.css";

export default ({ children }) => {
    return (
        <div className={styles.SiteContainer}>
            {children}
        </div>
    );
};