import React from "react";
import { graphql, StaticQuery } from "gatsby";
import Img from "gatsby-image";
import cn from "classnames";
import fa from "font-awesome/css/font-awesome.min.css";

import styles from './styles.module.css';

let year = new Date();

const Footer = () => (
    <StaticQuery
        query={graphql`
            query FesbLogo {
              image: file(name: {eq: "fesb-logo-white"}) {
                childImageSharp {
                  fluid {
                    aspectRatio
                    sizes
                    src
                    srcSet
                  }
                }
              }
            }
        `}
        render={
            props => {
                const { image: {
                    childImageSharp: { fluid }
                }} = props;

                return (
                    (
                        <footer>
                            <div className={styles.Copyright}>
                                <Img
                                    fluid={fluid}
                                    className={styles.FESB_Logo}
                                />
                                <span className={styles.Copyright_text}>
                                    Copyright &copy; Mateo Fuzul - { year.getFullYear() }
                                </span>
                            </div>
                        </footer>
                    )
                )
            }
        }
    />
);

export default Footer;
