import React from "react";

import SiteContainer from '../SiteContainer';
import Header from '../Header';
import Container from '../Container';
import Footer from "../Footer";
import Overlay from "../Header/Overlay";

class Layout extends React.Component {
    constructor(props) {
        super(props);

        this.toggleMenu = this.toggleMenu.bind(this);

        this.state = {
            showMenu: false
        }
    }

    toggleMenu() {
        this.setState({
            showMenu: !this.state.showMenu
        });
    }

    render() {
        const { toggleMenu } = this;
        const { children } = this.props;
        const { showMenu } = this.state;

        return (
            <SiteContainer>
                <Overlay
                    toggleMenu={toggleMenu}
                    showMenu={showMenu}
                />
                <Header
                    toggleMenu={toggleMenu}
                />
                <Container>
                    { children }
                </Container>
                <Footer />
            </SiteContainer>
        );
    }
}

export default Layout;