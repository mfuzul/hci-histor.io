import React from 'react';
import { graphql, StaticQuery } from "gatsby";
import styles from './styles.module.css';
import { SearchForm } from "../index";

class SearchComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <StaticQuery
                query={graphql`
                    query cities
                    {
                      allMarkdownRemark(
                        filter: { fileAbsolutePath: { regex: "/\\/museums\\//"}}
                        sort: { order: DESC, fields: [frontmatter___city] }
                      ) {
                        cities: distinct(field: frontmatter___city)
                      }
                    }
                `}
                render={ data => {
                    const { allMarkdownRemark: {cities: suggestions} } = data;

                    return (
                            <div className={styles.Search_Container}>
                                <span className={styles.Search_Title}>
                                    <i className={'fa fa-university fa-2x'}></i>
                                    <span>Find a museum, anywhere, anytime.</span>
                                </span>
                                <SearchForm
                                    suggestions={suggestions}
                                />
                            </div>
                        );
                    }
                }
            />
        );
    }
}

export default SearchComponent;