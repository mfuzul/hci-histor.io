import React from "react";
import AutoCompleteInput from "../Form/AutoCompleteInput/index";
import CustomDatePicker from "../Form/CustomDatePicker/index";
import _ from "../Form";

import "react-datepicker/dist/react-datepicker.min.css";
import styles from "./styles.module.css";

class SearchForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: new Date()
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    handleSearchBtnClick(e) {
        e.preventDefault();

        const cityInput = document.querySelector('input[name=city]');

        if('' === cityInput.value.trim()) {
            cityInput.classList.add('Invalid_Value');
            cityInput.placeholder = 'City field cannot be empty.';
        } else {
            document.querySelector('form').submit();
        }
    }

    render() {
        const {
            props: { suggestions, ...rest },
            state: { startDate },
            handleChange,
            handleSearchBtnClick
        } = this;
        const minDate = new Date();

        let displayInline = false;
        let cityValue = '';

        if (rest.hasOwnProperty('displayInline')) {
            displayInline = rest.displayInline;
        }

        if (rest.hasOwnProperty('cityValue')) {
            cityValue = rest.cityValue;
        }

        return (
            <_.Form
                className={displayInline ? styles.Search_Form_Inline : styles.Search_Form}
                action={'/search-results'}
                method={'GET'}
            >
                <AutoCompleteInput
                    placeholder={'City'}
                    name={'city'}
                    value={cityValue}
                    suggestions={suggestions}
                />
                <CustomDatePicker
                    onChange={handleChange}
                    selected={startDate}
                    minDate={minDate}
                />
                <_.Button
                    type={'submit'}
                    className={styles.Search_Btn}
                    onClick={handleSearchBtnClick}
                >
                    <i className={'fa fa-search'}></i> Search
                </_.Button>
            </_.Form>
        );
    }
}

export default SearchForm;
