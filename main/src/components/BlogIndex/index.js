import React from "react";
import { Link } from "gatsby";
import Img from "gatsby-image";
import styles from "./styles.module.css";
import _ from "../Form";
import { SidebarWidget } from "../index";
import LatestPost from '../SidebarWidget/LatestPost';

export default ({posts}) => {
    // posts list
    let postsList = posts.map(post => {
        const { id, excerpt } = post.node;
        const { title, date, slug, indexImage } = post.node.frontmatter;

        return (
            <section key={id} className={styles.Post}>
                <Link to={`blog/${slug}`}>
                    <div className={styles.Post_Image}>
                            <h2 className={styles.Title}>{title}</h2>
                        <Img
                            fluid={indexImage.childImageSharp.fluid}
                            className={styles.Index_Image}
                        />
                    </div>
                </Link>
                <div className={styles.Post_Description}>
                    <p className={styles.Excerpt}>{excerpt}</p>
                    <Link to={`blog/${slug}`} className={styles.Read_More}>
                        <_.Button
                            classType={'read-more'}
                        >
                            Read more
                        </_.Button>
                    </Link>
                    <hr />
                    <span className={styles.Date}>{date}</span>
                </div>
            </section>
        );
    });

    let latestPostsList = posts.slice(0, 3).map(post => {
        return (<LatestPost post={post.node.frontmatter} />);
    });

    return (
        <div className={styles.Container_Blog}>
            <div className={styles.Blog_List}>
                {postsList}
            </div>
            <div className={styles.Sidebar}>
                <SidebarWidget>
                    <h2>Popular posts</h2>
                    { latestPostsList }
                </SidebarWidget>
            </div>
        </div>
    );
};