export { default as Search } from './Search/index';
export { default as SearchForm } from './Search/SearchForm'
export { default as BlogIndex } from './BlogIndex';
export { default as SidebarWidget } from './SidebarWidget';
export { default as MuseumResults } from './MuseumResults/index';
export { default as MuseumResultHeader } from './MuseumResults/Header';
export { default as MuseumCard } from './MuseumCard';
export { default as PrivateRoute } from "./Private/PrivateRoute";

const LoremIpsum = `
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Quisque pretium quam quis sapien sagittis auctor. Nam pretium pretium nunc. 
    Fusce nec justo in ipsum aliquam mollis. Proin aliquam lacus in ullamcorper venenatis. 
    Fusce posuere faucibus odio nec convallis. Vivamus efficitur sapien non euismod faucibus. 
    Vivamus faucibus semper leo, nec viverra magna eleifend eu. 
    Etiam risus nisl, luctus vel faucibus ac, rhoncus vel sapien. 
    Donec laoreet sapien arcu, vitae suscipit lorem fringilla vitae. 
    Vestibulum in neque non nibh pretium convallis. Vestibulum quis gravida ipsum. 
    Nullam a sem luctus eros rutrum aliquet fringilla sed enim. 
    Nulla consequat, velit auctor luctus auctor, eros nibh commodo massa, id consectetur ipsum neque vitae mauris. 
    Nam rhoncus tortor arcu, eget ullamcorper lectus rutrum vitae. 
    Nam et ipsum vel quam interdum hendrerit sit amet sed felis. 
    Donec euismod semper pharetra.
`;

export { LoremIpsum };