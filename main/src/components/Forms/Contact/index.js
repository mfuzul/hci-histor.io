import React from "react";
import cn from "classnames";
import styles from './styles.module.css';

import _ from '../../Form';

export default () => (
    <div className={cn('Form_Container', styles.Container_Contact)}>
        <_.Form className={styles.Contact_Form}>
            <_.Input
                type={'text'}
                name={'full-name'}
                placeholder={'Full name'}
            />
            <_.Input
                type={'text'}
                name={'e-mail'}
                placeholder={'E-mail'}
            />
            <_.Input
                type={'text'}
                name={'subject'}
                placeholder={'Subject'}
            />
            <_.TextArea
                name={'message'}
                placeholder={'Message...'}
                rows={10}
            />
            <_.Button
                type={'submit'}
                id={'contact-submit'}
                classType={'submit'}
                pull={'right'}
            >
                <i className={'fa fa-send'}></i> Send
            </_.Button>
        </_.Form>
        <div className={styles.Contact_Description}>
            If you have any problems related to the application and it's usage
        message us and we will do our best to provide you with the appropriate
        information in the shortest possible time.
        </div>
    </div>
);

