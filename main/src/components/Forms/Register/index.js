import React from "react";
import cn from "classnames";
import styles from "./styles.module.css";

import CustomDatePicker from '../../Form/CustomDatePicker/index';
import _ from '../../Form';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: null
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    render() {
        const { handleChange, state: { startDate } } = this;
        let maxDate = new Date();
        maxDate.setFullYear(maxDate.getFullYear() - 18);


        return (
            <>
                <div className={cn('Form_Container', styles.Container_Register)}>
                    <_.Form className={styles.Register_Form}>
                        <_.Input
                            type={'text'}
                            name={'full-name'}
                            placeholder={'Full name'}
                        />
                        <_.Input
                            type={'text'}
                            name={'e-mail'}
                            placeholder={'E-mail'}
                        />
                        <_.Input
                            type={'password'}
                            name={'password'}
                            placeholder={'Password'}
                        />
                        <_.Input
                            type={'password'}
                            name={'repeat-password'}
                            placeholder={'Repeat password'}
                        />
                        <_.Input
                            type={'text'}
                            name={'country'}
                            placeholder={'Country'}
                        />
                        <CustomDatePicker
                            placeholderText={'Birth date'}
                            onChange={handleChange}
                            selected={startDate}
                            maxDate={maxDate}
                        />
                        <_.Button
                            type={'submit'}
                            id={'register-submit'}
                            classType={'submit'}
                            pull={'right'}
                        >
                            <i className={'fa fa-send'}></i> Register
                        </_.Button>
                    </_.Form>
                </div>
            </>
        );
    }
}

export default RegisterForm;