import React from "react";
import { navigate } from "gatsby";
import cn from "classnames";
import styles from "./styles.module.css";

import _ from "../../Form";

class LoginButton extends React.Component {
    constructor(props) {
        super(props);
    }

    handleLogin = () => {
        this.props.handleLogin({username: 'john', password: 'pass'});
        navigate(this.props.private);
    };

    render() {
        const { valid, message } = this.props;

        return (
            <div
                className={cn(styles.Login_Form_Button, valid ? '' : styles.Login_Form_Button_Invalid)}
                onClick={valid ? this.handleLogin : undefined }
            >
                {!valid ? message : 'Login'}
            </div>
        );
    }
}

class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: 'Enter your credentials',
            validEmail: false,
            validPassword: false,
        };

        this.handleEmailInputChange = this.handleEmailInputChange.bind(this);
        this.handlePasswordInputChange = this.handlePasswordInputChange.bind(this);
    }

    handleEmailInputChange(e) {
        if (-1 === e.currentTarget.value.indexOf('@')) {
            this.setState({
                ...this.state,
                message: 'Please provide a valid e-mail',
                validEmail: false
            });
        } else if (!this.state.validPassword) {
            this.setState({
                ...this.state,
                message: 'Password can\'t be empty',
                validEmail: true
            });
        } else {
            this.setState({
                ...this.state,
                validEmail: true
            });
        }
    }

    handlePasswordInputChange(e) {
        if ('' === e.currentTarget.value) {
            this.setState({
                ...this.state,
                message: 'Password can\'t be empty',
                validPassword: false,
            });
        } else if(!this.state.validEmail) {
            this.setState({
                ...this.state,
                message: 'Please provide a valid e-mail',
                validPassword: true
            });
        } else {
            this.setState({
                ...this.state,
                validPassword: true
            });
        }
    }

    render() {
        const { props, handleEmailInputChange, handlePasswordInputChange } = this;
        const { validEmail, validPassword, message } = this.state;

        return (
            <>
                <div className={cn('Form_Container', styles.Container_Login)}>
                    <_.Form
                        className={styles.Login_Form}
                    >
                        <_.Input
                            type={'text'}
                            name={'e-mail'}
                            placeholder={'E-mail'}
                            onChange={handleEmailInputChange}

                        />
                        <_.Input
                            type={'password'}
                            name={'password'}
                            placeholder={'Password'}
                            onChange={handlePasswordInputChange}
                        />
                        <LoginButton message={message} valid={validEmail && validPassword} {...props} />
                    </_.Form>

                    <span className={styles.Forgot_Password}>Forgot password? Click me!</span>
                </div>
            </>
        );
    }
}

export default LoginForm;