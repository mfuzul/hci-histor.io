import React from "react";
import styles from "./styles.module.css";
import {Link} from "gatsby";

const TicketRow = ({ ticket }) => (
    <tr className={styles.Ticket_Card}>
        <td>
            <span className={styles.Ticket_Id}>Ticket ID:</span> { ticket.ticketId }
        </td>
        <td>
            <Link to={`/museum/${ticket.museumSlug}`} className={styles.Museum_Link}>
                { ticket.museumName }
            </Link>
        </td>
        <td><i className={'fa fa-user'}></i> { ticket.type }</td>
        <td><i className={'fa fa-money'}></i> { ticket.price }</td>
        <td><i className={'fa fa-info'}></i> { ticket.status }</td>
        <td>
            <span type={'button'} className={styles.Download_Btn}>
                <i className={'fa fa-download'}></i> Ticket
            </span>
            <span type={'button'} className={styles.Download_Btn}>
                <i className={'fa fa-download'}></i> Receipt
            </span>
        </td>
    </tr>
);

export default TicketRow;