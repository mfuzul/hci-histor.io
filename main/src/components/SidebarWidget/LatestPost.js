import React from "react";
import { Link } from "gatsby";
import styles from './styles.module.css';

export default ({ post }) => (
    <Link to={`blog/${post.slug}`}>
      <div className={styles.Sidebar_Single}>
          <span>{ post.title }</span>
      </div>
    </Link>
);