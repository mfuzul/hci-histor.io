import React from "react";
import cn from "classnames";
import styles from "./styles.module.css";

export default ({ children, className }) => (
    <aside className={cn(className, styles.Sidebar_Widget)}>
        {children}
    </aside>
);