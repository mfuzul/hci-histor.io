import React from 'react';
import { graphql, StaticQuery } from "gatsby";
import PropTypes from 'prop-types';
import styles from './styles.module.css';
import { SearchForm } from "../index";


class Header extends React.Component {
    static propTypes = {
        city: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { city, date } = this.props;

        return (
            <StaticQuery
                query={graphql`
                    query citiesResults
                    {
                      allMarkdownRemark(
                        filter: { fileAbsolutePath: { regex: "/\\/museums\\//"}}
                        sort: { order: DESC, fields: [frontmatter___city] }
                      ) {
                        cities: distinct(field: frontmatter___city)
                      }
                    }
                `}
                render={ data => {
                    const { allMarkdownRemark: {cities: suggestions} } = data;

                    return (
                        <div className={styles.Results_Header}>
                            <p className={styles.Visit_Banner}>
                                Visit museums in <em className={styles.City}> { city } </em> on <em className={styles.Date}>{ date }</em>
                            </p>
                            <div className={styles.Results_Search_Form}>
                                <SearchForm
                                    displayInline={true}
                                    cityValue={city}
                                    suggestions={suggestions}
                                />
                            </div>
                        </div>
                    );
                }}
            />
        );
    }
}

export default Header;