import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css';
import { MuseumCard } from "../index";

class MuseumResults extends React.Component {
    static propTypes = {
        museums: PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { museums } = this.props;
        let museumResults;

        if (museums.length) {
            museumResults = museums.map(museum => {
                return (
                    <MuseumCard
                        key={museum.id}
                        museum={museum}
                    />
                );
            });
        } else {
            museumResults = (
                <div className={styles.No_Results}>
                    There are no results at the moment.
                </div>
            );
        }

        return (
            <div className={styles.Museum_Results}>
                {museumResults}
            </div>
        );
    }
}

export default MuseumResults;