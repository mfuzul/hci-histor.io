import React from 'react';

import styles from './styles.module.css';

const NotFound = () => (
    <div className={styles.not_found_container}>
        <img src={'images/404-animation.gif'} />
    </div>
)

export default NotFound