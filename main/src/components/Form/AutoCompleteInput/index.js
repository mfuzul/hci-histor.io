import React from 'react';
import PropTypes from 'prop-types';
import cn from "classnames";

import styles from '../styles.module.css';

class AutoCompleteInput extends React.Component {
    static propTypes = {
        name: PropTypes.string,
        placeholder: PropTypes.string,
        iconName: PropTypes.string,
        suggestions: PropTypes.instanceOf(Array)
    };

    static defaultProps = {
        name: '',
        placeholder: '',
        iconName: '',
        suggestions: []
    };

    constructor(props) {
        super(props);

        const { value } = props;

        this.state = {
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: value
        };
    }

    onChange = e => {
        const { suggestions } = this.props;
        const inputElement = e.currentTarget;
        const userInput = inputElement.value;

        if ('' === userInput.trim()
        ) {
            inputElement.classList.add('Invalid_Value')
        } else if (inputElement.classList.contains('Invalid_Value')) {
            inputElement.classList.remove('Invalid_Value');
        }

        const filteredSuggestions = suggestions.filter(
            suggestion => suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );

        this.setState({
            activeSuggestion: 0,
            filteredSuggestions,
            showSuggestions: true,
            userInput: userInput
        });
    };

    onClick = e => {
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: true,
            userInput: e.currentTarget.innerText
        });
    };

    onKeyDown = e => {
        const { activeSuggestion, filteredSuggestions } = this.state;

        switch (e.keyCode) {
            case 13:
                e.preventDefault();

                this.setState({
                   activeSuggestion: 0,
                   showSuggestions: false,
                   userInput: filteredSuggestions[activeSuggestion]
                });

                break;
            case 38:
                if (0 === activeSuggestion) {
                    return;
                }

                this.setState({ activeSuggestion: activeSuggestion - 1 });

                break;
            case 40:
                if (activeSuggestion - 1 === filteredSuggestions.length) {
                    return;
                }

                this.setState({ activeSuggestion: activeSuggestion + 1 });

                break;
        }
    };

    onFocus = e => {
        this.setState({
           ...this.state,
           showSuggestions: '' !== e.currentTarget.value.trim()
        });
    };

    onBlur = () => {
        this.setState({
            ...this.state,
            showSuggestions: false
        })
    };

    render() {
        const {
            onChange,
            onClick,
            onKeyDown,
            onFocus,
            onBlur,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        let suggestionsListComponent;

        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <ul className={cn(styles.Suggestions, styles.Show_Suggestions)}>
                        {filteredSuggestions.map((suggestion, index) => {
                            let className;

                            if (index === activeSuggestion) {
                                className = styles.Suggestion_Active;
                            }

                            return (
                                <li
                                    className={className}
                                    key={suggestion}
                                    onMouseDown={onClick}
                                >
                                    {suggestion}
                                </li>
                            );
                        })}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div className={cn(styles.Suggestions, styles.No_Suggestions)}>
                        <em>No suggestions</em>
                    </div>
                );
            }
        }

        return (
            <div className={styles.Form_Group}>
                <input type={'text'}
                       name={this.props.name}
                       placeholder={this.props.placeholder}
                       onChange={onChange}
                       onKeyDown={onKeyDown}
                       onFocus={onFocus}
                       onBlur={onBlur}
                       value={userInput}
                />
                {suggestionsListComponent}
            </div>
        )
    }
}

export default AutoCompleteInput;