import React from "react";
import styles from "../styles.module.css";
import DatePicker from "react-datepicker/es";

class CustomDatePicker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            datePickerComponent: null
        };
    }

    componentDidMount() {
        const {
            placeholderText,
            minDate,
            maxDate,
            selected,
            onChange
        } = this.props;

        this.setState({
            datePickerComponent: (
                <DatePicker
                    className={styles.Custom_DatePicker}
                    dropdownMode={'select'}
                    minDate={minDate}
                    maxDate={maxDate}
                    name={'date'}
                    onChange={onChange}
                    placeholderText={placeholderText ? placeholderText : 'Date'}
                    selected={selected}
                    showDisabledMonthNavigation={false}
                    showMonthDropdown={true}
                    showYearDropdown={true}
                    dateFormat={'d.M.Y.'}
                />
            )
        });
    }

    render() {
        const { label } = this.props;
        const { datePickerComponent } = this.state;

        let labelComponent = label ?
            (
                <div className={styles.Label}>
                    <label htmlFor={'date'}>{label}</label>
                </div>
            ) :
            undefined
        ;

        return (
            <div className={styles.Form_Group}>
                {labelComponent}
                {datePickerComponent}
            </div>
        );
    }
}

export default CustomDatePicker;