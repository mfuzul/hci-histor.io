import React from 'react';
import DatePicker from "react-datepicker";

import styles from './styles.module.css';
import cn from 'classnames';

const Form = ({children, action, method, id, className}) => (
    <form
        id={id}
        action={action}
        method={method}
        className={cn(className)}
        autoComplete={'off'}
    >
        {children}
    </form>
);

const Input = ({type, name, placeholder, label, iconName, ...rest}) => {
    let labelComponent = (undefined !== label && '' !== label) ?
        (
            <div className={styles.Label}>
                <label htmlFor={name}>{label}</label>
            </div>
        ) :
        '';

    let iconComponent = (undefined !== iconName) && '' !== iconName ?
        (
            <span className={'icon-placeholder'}>
                <i className={`fa fa-${iconName}`}></i>
            </span>
        ) :
        '';

    return (
        <div className={cn(styles.Form_Group, rest.className)}>
            {labelComponent}
            <input
                type={type}
                placeholder={placeholder}
                name={name}
                onChange={rest.onChange}
            />
            {iconComponent}
        </div>
    );
};

const TextArea = ({name, placeholder, label, cols, rows, ...rest}) => {
    let labelComponent = (undefined !== label && '' !== label) ?
        (
            <div className={styles.Label}>
                <label htmlFor={name}>{label}</label>
            </div>
        ) :
        '';

    return (
        <div className={styles.Form_Group}>
            {labelComponent}
            <textarea placeholder={placeholder} cols={cols} rows={rows}></textarea>
        </div>
    );
}

// Send options as { name: string, value: any }
const Select = ({id, name, placeholder, label, children, ...rest}) => {
    const labelComponent = label ?
        (
            <div className={styles.Label}>
                <label htmlFor={name}>{label}</label>
            </div>
        ) :
        '';

    return (
        <div className={styles.Form_Group}>
            {labelComponent}
            <select
                id={id}
                name={name}
            >
                { children }
            </select>
        </div>
    )
};

const pullSides = {
    'right': styles.Pull_Right,
    'left': styles.Pull_Left
};

const Button = ({
    children,
    type,
    id,
    className,
    classType,
    pull,
    onClick
}) => (
    <div className={styles.Form_Group}>
        <button
            id={id}
            type={type}
            className={cn(styles.Btn, className, styles.Btn_Submit, pullSides[pull])}
            onClick={onClick}
        >
            {children}
        </button>
    </div>
);

export default {
    Form,
    Input,
    TextArea,
    Button,
    Select
};

export { default as Contact } from '../Forms/Contact';