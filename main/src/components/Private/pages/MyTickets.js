import React from "react";
import styles from "../styles/MyTickets.module.css";
import TicketRow from "../../TicketRow";

export default () => {
    const ticketData = require('./my-tickets.json');

    const ticketRowsComponent = ticketData.map(ticket => {
        return <TicketRow ticket={ticket} />
    });

    return (
      <div className={styles.MyTickets_Container}>
          <h1>My tickets</h1>
          <table className={styles.Tickets_Table}>
              <thead>
              <tr>
                  <th>Ticket ID</th>
                  <th>Museum name</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Purchase status</th>
                  <th>Download</th>
              </tr>
              </thead>
              <tbody>
                { ticketRowsComponent }
              </tbody>
          </table>
      </div>
    )
};