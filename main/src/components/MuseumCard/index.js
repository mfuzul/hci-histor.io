import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "gatsby";
import cn from 'classnames';

import styles from './styles.module.css';
import moment from "moment";

class MuseumCard extends React.Component {
    static propTypes = {
        museum: PropTypes.object.isRequired
    };

    static defaultProps = {
        museum: {}
    };

    render() {
        const { museum: { name, city, price, slug } } = this.props;
        let date = (new moment()).format('DD.MM.YYYY.');

        return (
            <Link to={`/museum/${slug}`} className={styles.Museum_Card_Wrapper}>
                <div className={styles.Museum_Card}>
                    <span className={styles.Museum_Name}>
                        <i className={'fa fa-university'}></i> { name }
                    </span>
                    <span className={styles.Location}>
                        <i className={'fa fa-map-marker'}></i> {city}
                    </span>
                    <span className={styles.Price}>
                        <i className={'fa fa-money'}></i> { price }
                    </span>
                    <Link
                        to={`/buy?museum=${slug}&date=${date}`}
                        className={cn('Btn', styles.Button_Buy)}
                    >
                        Buy
                    </Link>
                </div>
            </Link>
        );
    }
}

export default MuseumCard;