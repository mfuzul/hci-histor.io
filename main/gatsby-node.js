/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require("path");

exports.createPages = ({ actions, graphql }) => {
    const { createPage } = actions;

    const postTemplate = path.resolve(`src/templates/BlogPost/index.js`);
    const museumTemplate = path.resolve(`src/templates/Museum/index.js`);

    return graphql(`
    {
      posts: allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/\\/content\\/posts\\//"}}) {
        edges {
          node {
            frontmatter {
              slug
            }
          }
        }
      }
      museums: allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/\\/content\\/museums\\//"}}) {
        edges {
          node {
            frontmatter {
                slug
            }
          }
        }
      }
    }
  `).then(result => {
        if (result.errors) {
            return Promise.reject(result.errors);
        }

        result.data.posts.edges.forEach(({ node }) => {
            const { slug } = node.frontmatter;
            createPage({
                path: "/blog/" + slug,
                component: postTemplate,
                context: {
                    // additional data can be passed via context
                    slug
                }
            });
        });

        result.data.museums.edges.forEach(({ node }) => {
            const { slug } = node.frontmatter;
            createPage({
                path: `/museum/${slug}`,
                component: museumTemplate,
                context: {
                    slug
                }
            });
        });
    });
};

exports.onCreateWebpackConfig = ({ stage, actions }) => {
    actions.setWebpackConfig({
        resolve: {
            modules: [path.resolve(__dirname, "./src")]
        }
    });
};

exports.onCreatePage = async ({ page, actions }) => {
    const { createPage } = actions;

    // page.matchPath is a special key that's used for matching pages
    // only on the client.
    if (page.path.match(/^\/private/)) {
        page.matchPath = "/private/*";

        // Update the page.
        createPage(page);
    }
};